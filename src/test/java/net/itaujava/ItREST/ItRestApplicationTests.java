package net.itaujava.ItREST;



import static org.junit.jupiter.api.Assertions.assertNotNull;



import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import itauteste.config.DBconfig;

@SpringBootTest
class ItRestApplicationTests {

	@Test
	@DisplayName("Testa DB")
	void testeDB() {
		try {
			assertNotNull(DBconfig.getConnection());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package itauteste.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import itauteste.dao.CidadesDAO;
import itauteste.dtos.ParCidadesDTO;
import itauteste.entidade.Cidade;
import itauteste.entidade.ParCidade;

@Service
public class CidadeService {

	@Autowired
	CidadesDAO cidadesDAO;

	public List<Cidade> listarCidades() {
		List<Cidade> lista = null;
		try {
			lista = cidadesDAO.listarCidades();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public List<ParCidade> combinarCidades(List<Cidade> listaCidade) {
		List<ParCidade> listaParesCidades = new ArrayList<ParCidade>();
		ParCidade parCidade = new ParCidade();
		// Ao utilizar uma matriz para combinar, é possível minimizar o trabalho
		// necessário e evitar repetições de calculo
		for (int i=0; i < listaCidade.size(); i++) {
			//Compara-se uma cidade com a subsquente na lista, assim sucessivamente
			// para cada nova combinação
			parCidade.setcidade1(listaCidade.get(i));
			for (int j=i+1; j < listaCidade.size(); j++) {
				parCidade.setcidade2(listaCidade.get(j));
				listaParesCidades.add(parCidade);
			}
		}
		return listaParesCidades;
	}
	public List<ParCidadesDTO> obterDistanciaParesCidades(List<ParCidade> listaParCidades, String unidade){
		List<ParCidadesDTO> parCidadeCalculada = new ArrayList<ParCidadesDTO>();
		for (int i = 0; i<listaParCidades.size(); i++) {
			parCidadeCalculada.add(new ParCidadesDTO(listaParCidades.get(i).getcidade1(), listaParCidades.get(i).getcidade2(), listaParCidades.get(i).calcularDistanciaComHaversine(unidade),unidade));
		}
		return parCidadeCalculada;
	}
}

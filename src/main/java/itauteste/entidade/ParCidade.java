package itauteste.entidade;

public class ParCidade {
	private Cidade cidade1, cidade2;
	
	public double calcularDistanciaComHaversine(String unidade) {
		// O metodo de Haversine foi escolhido pois é metodo utilizado para  
		// sistemas de GPS, o mesmo considera o raio da terra para realizar os calculos, 
		// assim trazendo maior precisão para o resultado
		final int R = 6371; // Raio da terra
		Double latitudeDistancia = converterRadiano(cidade2.getLatitude() - cidade1.getLatitude());
		Double longitudeDistancia = converterRadiano(cidade2.getLongitude() - cidade1.getLongitude());
		Double a = Math.sin(latitudeDistancia / 2) * Math.sin(latitudeDistancia / 2)
				+ Math.cos(converterRadiano(cidade1.getLatitude())) * Math.cos(converterRadiano(cidade2.getLatitude()))
						* Math.sin(longitudeDistancia / 2) * Math.sin(longitudeDistancia / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distancia = R * c;
		if (unidade =="mi") {
			return distancia*0.621371192;
		}
		return distancia;
	}
	
	private Double converterRadiano(Double valor) {
		return valor * Math.PI / 180;
	}

	public Cidade getcidade1() {
		return cidade1;
	}
	public void setcidade1(Cidade cidade1) {
		this.cidade1 = cidade1;
	}
	public Cidade getcidade2() {
		return cidade2;
	}
	public void setcidade2(Cidade cidade2) {
		this.cidade2 = cidade2;
	}
}

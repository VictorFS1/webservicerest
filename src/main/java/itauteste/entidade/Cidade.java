package itauteste.entidade;

import java.io.Serializable;

public class Cidade implements Serializable{
	private int id;
	private String cidadeNome;
	private double latitude;
	private double longitude;
	
	public Cidade(int id, String cidadeNome, double latitude, double longitude) {
		super();
		this.id = id;
		this.cidadeNome = cidadeNome;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Cidade() {
		super();
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getcidadeNome() {
		return cidadeNome;
	}
	public void setcidadeNome(String cidadeNome) {
		this.cidadeNome = cidadeNome;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return cidadeNome;
	}
	
}
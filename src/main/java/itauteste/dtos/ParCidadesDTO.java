package itauteste.dtos;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import itauteste.entidade.Cidade;


@JacksonXmlRootElement
public class ParCidadesDTO implements Serializable {
	@JacksonXmlProperty 
	private Cidade cidade1, cidade2;
	private double distancia;
	private String unidade;
	
	public ParCidadesDTO(Cidade cidade1, Cidade cidade2, double distancia, String unidade) {
		super();
		this.cidade1 = cidade1;
		this.cidade2 = cidade2;
		this.distancia = distancia;
		this.unidade = unidade;
	}


	public Cidade getCidade1() {
		return cidade1;
	}

	public void setCidade1(Cidade cidade1) {
		this.cidade1 = cidade1;
	}

	@Override
	public String toString() {
		return "ParCidadesDTO [cidade1=" + cidade1 + ", cidade2=" + cidade2 + ", distancia=" + distancia + ", unidade="
				+ unidade + "]";
	}


	public Cidade getCidade2() {
		return cidade2;
	}

	public void setCidade2(Cidade cidade2) {
		this.cidade2 = cidade2;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
}

package itauteste.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import itauteste.config.DBconfig;
import itauteste.entidade.Cidade;

@Component
public class CidadesDAO {

	public List<Cidade> listarCidades() throws Exception {
		List<Cidade> lista = new ArrayList<>();
	
		Connection conexao = DBconfig.getConnection();
		
		String sql = "SELECT * FROM city";
		
		PreparedStatement statement = conexao.prepareStatement(sql);
		ResultSet result = statement.executeQuery();
		
		while (result.next()) {
			Cidade cidade = new Cidade();
			cidade.setId(result.getInt("id"));
			cidade.setcidadeNome(result.getString("city_name"));
			cidade.setLatitude(result.getDouble("latitude"));
			cidade.setLongitude(result.getDouble("longitude"));
			
			lista.add(cidade);
		}
		return lista;
	}
}

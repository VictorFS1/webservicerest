package net.itaujava.ItREST;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import itauteste.dtos.ParCidadesDTO;
import itauteste.entidade.Cidade;
import itauteste.entidade.ParCidade;
import itauteste.services.CidadeService;

@RestController
public class WsController {

	@RequestMapping("/cidades")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ParCidadesDTO>> listarParesCidadeComDistancia(
			@RequestParam(value = "unit", defaultValue = "km") String unit,
			@RequestParam(value = "responseType", defaultValue = "json") String responseType) throws Exception {
		CidadeService service = new CidadeService();
		List<Cidade> listaCidades = service.listarCidades();
		List<ParCidade> combinarCidade = service.combinarCidades(listaCidades);
		List<ParCidadesDTO> paresCidadesObtidos = service.obterDistanciaParesCidades(combinarCidade, unit);
		if (responseType.equals("xml")) {
			return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_XML).body(paresCidadesObtidos);
		}
		else if (responseType.equals("json")) {
			return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(paresCidadesObtidos);
		}
		else {
			return ResponseEntity.badRequest().build();
		}
	}
}
